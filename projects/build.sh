#!/bin/bash

home = "${PWD}"
for D in *; do
    if [ -d "${D}" ]; then
        echo "Building ${D}"
        cd "${D}";./gradlew build
    fi
done

echo done!
